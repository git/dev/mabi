# Copyright 1999-2009 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

WANT_AUTOTOOLS="2.5"
inherit autotools mono

# "." is not allowed as part of a package name
MY_PN="mpi.net"
MY_P="${MY_PN}-${PV}"
S="${WORKDIR}/${MY_P}"

DESCRIPTION="C# bindings for various MPI-implementations"
HOMEPAGE="http://www.osl.iu.edu/research/mpi.net"
SRC_URI="http://www.osl.iu.edu/research/mpi.net/files/${PV}/${MY_P}.tar.gz"

LICENSE="Boost"
SLOT="0"
KEYWORDS="~ppc"
IUSE=""

DEPEND="virtual/mpi
		>=dev-lang/mono-2.0"
RDEPEND="${DEPEND}"

MPICC="${CC}"

src_unpack() {
	unpack ${A}
	cd "${S}"

	epatch "${FILESDIR}/configure.ac.patch"
	epatch "${FILESDIR}/Makefile.am.patch"
	epatch "${FILESDIR}/Unsafe.pl.patch"

	# MPI/Makefile seems broken, fix it
	eautoreconf
}

src_compile() {
	# policy requires us to build shared and static libs alongside
	econf --enable-shared --enable-static
	emake
}

src_install() {
	emake DESTDIR="${D}" install || die "Install failed"
}

src_test() {
	make check -k
}
